#!/usr/bin/env bash

source "$HOME/.dotfiles/functions/text_format.sh"

link_files() {
  ln -s "$1" "$2"
  e_success "linked $1 to $2"
}

install_sshfiles() {
  e_header "Linking .ssh files"

  SSH_FILES="$(cd "$(dirname "$0")"; pwd)"

  for source in `find $SSH_FILES -maxdepth 2 -name \*.deploy`; do
    dest="$HOME/.ssh/`basename \"${source%.*}\"`"

    if [ -f $dest ] || [ -d $dest ]; then
      mv $dest{,.bak}
      e_success "moved $dest to $dest.bak"
    fi
    link_files $source $dest
  done
}

install_sshfiles
