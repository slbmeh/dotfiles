# Make sure we’re using the latest Homebrew
update

# Upgrade any already-installed formulae
upgrade

# Install GNU core utilities (those that come with OS X are outdated)
# Don’t forget to add `$(brew --prefix coreutils)/libexec/gnubin` to `$PATH`.
install coreutils
# Install GNU `find`, `locate`, `updatedb`, and `xargs`, g-prefixed
install findutils
# Install Bash 4
install bash

# Install wget with IRI support
install wget --enable-iri

# Install more recent versions of some OS X tools
install vim --override-system-vi
tap homebrew/dupes
install homebrew/dupes/grep
tap josegonzalez/homebrew-php
install php55
install homebrew/dupes/screen

# Install other useful binaries
install ack
install exiv2
install git
install imagemagick
install lynx
install node
install pigz
install rename
install rhino
install tree
install webkit2png
install zopfli

tap homebrew/versions
install lua52

# Install native apps
tap phinze/homebrew-cask
install brew-cask

cask install android-studio
cask install arduino
cask install balsamiq-mockups
cask install blender
cask install caffeine
cask install ccmenu
cask install chainsaw
cask install chromium
cask install colorpicker-developer
cask install copy
cask install dia
cask install dropbox
cask install evernote
cask install firefox
cask install flow
cask install freemind
cask install ganttproject
cask install github
cask install gitifier
cask install google-chrome
cask install google-chrome-canary
cask install google-drive
cask install google-earth
cask install google-hangouts
cask install hamachi
cask install imageoptim
cask install inkscape
cask install iphone-configuration-utility
cask install libreoffice
cask install limechat
cask install mactex
cask install mysql-workbench
cask install netbeans
cask install opera
cask install opera-next
cask install qt-creator
cask install rescuetime
cask install seamonkey
cask install sketchup
cask install unity3d
cask install vlc

# Remove outdated versions from the cellar
cleanup
