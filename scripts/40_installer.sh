#!/usr/bin/env bash

cd

e_header "Running individual installers"

find $DOTFILES_ROOT -name install.sh | while read installer ; do
  if [[ -x "$installer" ]] ; then
    bash -c "$installer"
    e_success "Completed execution of $installer"
  else
    e_success "Skipping $installer because it is not marked executable."
  fi
done
