#!/usr/bin/env bash

link_files() {
  ln -s "$1" "$2"
  e_success "linked $1 to $2"
}

setup_gitconfig() {
  if ! [ -f $DOTFILES_ROOT/vcs/gitconfig.symlink ]; then
    e_header "Configuring git template"

    git_credential='cache'
    if [[ "$OSTYPE" =~ ^darwin ]]; then
      git_credential='osxkeychain'
    fi

    a_prompt " - What is your git author name?"
    read -e git_authorname

    a_prompt " - What is your git author email?"
    read -e git_authoremail

    git_version="$(git --version | cut -d' ' -f3)"
    simple_capable="$(verlt 1.7.11 $git_version && echo \"yes\" || echo \"no\")"

    git_pushdefault='simple'
    if [[ $simple_capable != "yes" ]] ; then
      git_pushdefault='current'
    fi

    sed -e "s/AUTHORNAME/$git_authorname/g" -e "s/AUTHOREMAIL/$git_authoremail/g" -e "s/GIT_CREDENTIAL_HELPER/$git_credential/g" -e "s/GIT_PUSH_DEFAULT/$git_pushdefault/g" $DOTFILES_ROOT/vcs/gitconfig.template > $DOTFILES_ROOT/vcs/gitconfig.symlink

    e_success "generated git configuration"
  fi
}

install_dotfiles() {
  e_header "Linking dotfiles"

  overwrite_all=false
  backup_all=false
  skip_all=false

  if [[ -n "$DOTFILES_CHEF" ]] ; then
    backup_all=true
  fi

  for source in `find $DOTFILES_ROOT -maxdepth 2 -name \*.symlink`; do
    dest="$HOME/.`basename \"${source%.*}\"`"

    if [ -f $dest ] || [ -d $dest ]; then
      if [[ "$source" != "$(readlink -f "$dest" 2>/dev/null)" ]] ; then
        overwrite=false
        backup=false
        skip=false

        if [ "$overwrite_all" == "false" ] && [ "$backup_all" == "false" ] && [ "$skip_all" == "false" ]; then
          a_prompt "File already exists: `basename $source`, what do you want to do? [s]kip, [S]kip all, [o]verwrite, [O]verwrite all, [b]ackup, [B]ackup all?"
          read -n 1 action
          echo ''

          case "$action" in
            o )
              overwrite=true;;
            O )
              overwrite_all=true;;
            b )
              backup=true;;
            B )
              backup_all=true;;
            s )
              skip=true;;
            S )
              skip_all=true;;
            * )
              ;;
          esac
        fi

        if [ "$overwrite" == "true" ] || [ "$overwrite_all" == "true" ]; then
          rm -rf $dest
          e_success "removed $dest"
        fi

        if [ "$backup" == "true" ] || [ "$backup_all" == "true" ]; then
          mv $dest{,.bak}
          e_success "moved $dest to $dest.bak"
        fi

        if [ "$skip" == "false" ] && [ "$skip_all" == "false" ]; then
          link_files $source $dest
        else
          e_success "skipped $source"
        fi
      else
          e_success "$source is alredy linked to $dest"
      fi
    else
      link_files $source $dest
    fi
  done
}

if [[ -n "$DOTFILES_CHEF" ]] ; then
  touch "$DOTFILES_ROOT/reinstall"
else
  setup_gitconfig
fi

install_dotfiles
