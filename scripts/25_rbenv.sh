#!/usr/bin/env bash

if [[ ! -d ~/.rbenv ]]; then
  e_header "Installing rbenv"
  git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
else 
  e_header "Updating rbenv"
  cd ~/.rbenv
  git pull
fi

PATH="~/.rbenv/bin:$(path_remove ~/.rbenv/bin)"
eval "$(rbenv init -)"

if [[ ! -d ~/.rbenv/plugins/ruby-build ]]; then
  e_header "Installing ruby-build"
  git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
else
  e_header "Updating ruby-build"
  cd ~/.rbenv/plugins/ruby-build
  git pull
fi

rbenv rehash
