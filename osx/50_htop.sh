#!/usr/bin/env bash
[[ "$(type -P brew)" ]] || return 1

source "$HOME/.dotfiles/functions/text_format.sh"

binroot="$(brew --config | awk '/HOMEBREW_PREFIX/ {print $2}')/bin"

if [[ ! "$(type -P $binroot/htop)" ]] ; then
  e_header "Installing htop"
  brew install htop-osx
fi

if [[ "$(type -P $binroot/htop)" && "$(stat -L -f "%U:%G" "$binroot/htop")" != "root:wheel" || ! -u "$binroot/htop" ]] ; then
  e_header "Fixing htop permissions"
  sudo chown root:wheel "$binroot/htop"
  sudo chmod u+s "$binroot/htop"
fi
