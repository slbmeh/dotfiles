#!/usr/bin/env bash
[[ "$(type -P brew)" ]] || return 1

source "$HOME/.dotfiles/functions/text_format.sh"

binroot="$(brew --config | awk '/HOMEBREW_PREFIX/ {print $2}')/bin"

if [[ ! "$(type -P $binroot/bash)" ]] ; then
  e_header "Installing bash"
  brew install bash
fi

if [[ "$(type -P $binroot/bash)" && "$(cat /etc/shells | grep -q "$binroot/bash")" ]] ; then
  e_header "Adding $binroot/bash to the list of acceptable shells"
  echo "$binroot/bash" | sudo tee -a /etc/shells >/dev/null
fi

if [[ "$SHELL" != "$binroot/bash" ]] ; then
  e_header "Making $binroot/bash your default shell"
  sudo chsh -s "$binroot/bash" "$USER" > /dev/null 2>&1
  e_arrow "Please exit and restart all your shells."
fi
