#!/usr/bin/env bash
[[ "$(type -P brew)" ]] || return 1

source "$HOME/.dotfiles/functions/text_format.sh"

OSX_DIR="$(cd "$(dirname "$0")"; pwd)"

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for bundle in `find $OSX_DIR -maxdepth 2 -name \*.bundle` ; do
  brew bundle $bundle
  e_success "Installed brew bundle in $bundle."
done
IFS=$SAVEIFS
