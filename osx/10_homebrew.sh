#!/usr/bin/env bash
source "$HOME/.dotfiles/functions/text_format.sh"

if [[ ! "$(type -P brew)" ]] ; then
  e_header "Installing Homebrew"
  ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)" > /tmp/homebrew-install.log
fi

if [[ "$(type -P brew)" ]] ; then
  e_header "Updating homebrew"
  brew update
  brew upgrade
fi
