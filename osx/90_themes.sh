#!/usr/bin/env bash

source "$HOME/.dotfiles/functions/text_format.sh"

OSX_DIR="$(cd "$(dirname "$0")"; pwd)";

function install_terminal_themes() {
  local SAVEIFS=$IFS
  IFS=$(echo -en "\n\b")
  for theme in `find $OSX_DIR/themes -maxdepth 2 -name \*.terminal`; do
    escaped_theme="$(echo "$theme" | sed -e 's/ /\\ /g')"
    theme_name="$(basename "${escaped_theme%.*}")"
    defaults find $theme_name | grep -i "come.apple.terminal" >/dev/null
    if [[ $? -eq 0 ]] ; then
      open -a /Applications/Utilities/Terminal.app "$theme"
      sleep 1
      /usr/libexec/PlistBuddy -c "Add: :Window\ Settings:$theme_name:useOptionAsMetaKey integer 1" ~/Library/Preferences/com.apple.Terminal.plist >/dev/null
      /usr/libexec/PlistBuddy -c "Add: :Window\ Settings:$theme_name:BackgroundSettingsForInactiveWindows integer 1" ~/Library/Preferences/com.apple.Terminal.plist >/dev/null
      /usr/libexec/PlistBuddy -c "Add: :Window\ Settings:$theme_name:BackgroundAlphaInactive real 0.5" ~/Library/Preferences/com.apple.Terminal.plist >/dev/null
      e_success "Installed Terminal.app theme '$theme'."
    fi
  done
  IFS=$SAVEIFS
}

function install_iterm2_themes() {
  local SAVEIFS=$IFS
  IFS=$(echo -en "\n\b")
  for theme in `find $OSX_DIR/themes -maxdepth 2 -name \*.itermcolors`; do
    escaped_theme="$(echo "$theme" | sed -e 's/ /\\ /g')"
    theme_name="$(basename "${escaped_theme%.*}")"
    defaults find $theme_name | grep -i "com.googlecode.iterm2"
    if [[ $? -eq 0 ]] ; then
      /usr/libexec/PlistBuddy -c "Add :Custom\ Color\ Presets:$theme_name dict" ~/Library/Preferences/com.googlecode.iterm2.plist 2>/dev/null
      /usr/libexec/PlistBuddy -c "Merge $escaped_theme :Custom\ Color\ Presets:$theme_name" ~/Library/Preferences/com.googlecode.iterm2.plist >/dev/null
      e_success "Installed iTerm.app theme '$theme'."
    fi
  done
  IFS=$SAVEIFS
}

e_header "Installing Terminal.app themes"
install_terminal_themes
sleep 1
defaults write com.apple.terminal "Default Window Settings" -string "Solarized Dark"
defaults write com.apple.terminal "Startup Window Settings" -string "Solarized Dark"
e_success "Set default Terminal.app theme to Solarized Dark"


e_header "Installing iTerm.app themes"
install_iterm2_themes
