#!/usr/bin/env bash

source "$HOME/.dotfiles/functions/text_format.sh"

OSX_DIR="$(cd "$(dirname "$0")"; pwd)";

function install_iterm_keymap() {
  /usr/libexec/PlistBuddy -c "Merge $OSX_DIR/default.itermkeymap :New\ Bookmarks:0:Keyboard\ Map" ~/Library/Preferences/com.googlecode.iterm2.plist >/dev/null
  e_success "Installed iTerm.app default keymap."
}

e_header "Installing iTerm.app Keyboard Map"
install_iterm_keymap
