#!/usr/bin/env bash
[[ "$OSTYPE" =~ ^darwin ]] || exit 0

source "$HOME/.dotfiles/functions/text_format.sh"

function run_osx_installers() {
  OSX_INSTALLERS="$(cd "$(dirname "$0")"; pwd)";

  local SAVEIFS=$IFS
  IFS=$(echo -en "\n\b")
  for installer in `find $OSX_INSTALLERS -maxdepth 2 -name \*.sh -and ! -name install.sh`; do
    if [[ -x "$installer" ]] ; then
      bash -c "$installer"
    else
      e_success "Skipping $installer because it is not marked executable."
    fi
  done
  IFS=$SAVEIFS
}

e_header "Running OSX installers"
e_arrow "Requesting user password to cache credentials in sudo."

sudo -v

while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

run_osx_installers
