# From http://stackoverflow.com/questions/370047/#370255
function path_remove() {
  SAVEIFS=$IFS
  IFS=:
  # convert it to an array
  t=($PATH)
  unset IFS
  # perform any array operations to remove elements from the array
  t=(${t[@]%%$1})
  IFS=:
  # output the new array
  echo "${t[*]}"
  IFS=$SAVEIFS
}

if [[ "$OSTYPE" =~ ^darwin ]]; then
  SORT_EXEC="gsort"
else
  SORT_EXEC="sort"
fi

# From http://stackoverflow.com/questions/4023830/#4024263
verlte() {
    [  "$1" = "`echo -e "$1\n$2" | $SORT_EXEC -V | head -n1`" ]
}

verlt() {
    [ "$1" = "$2" ] && return 1 || verlte $1 $2
}
