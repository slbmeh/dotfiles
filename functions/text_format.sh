#!/usr/bin/env bash

# Logging stuff.                                                                                                                                                                             
function e_header()   { echo -e "\n\033[1m$@\033[0m"; }
function e_success()  { echo -e " \033[1;32m✔\033[0m  $@"; }
function e_error()    { echo -e " \033[1;31m✖\033[0m  $@"; }
function e_arrow()    { echo -e " \033[1;33m➜\033[0m  $@"; }
function a_prompt()   { printf "\r  [ \033[0;33m?\033[0m ] $1 "; }