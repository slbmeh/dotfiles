#!/usr/bin/env bash

source "$HOME/.dotfiles/functions/text_format.sh"

copy_files() {
  cp "$1" "$2"
  e_success "copied $1 to $2"
}

install_fontfiles() {
  e_header "Copying .ttf files"

  FONT_FILES="$(cd "$(dirname "$0")"; pwd)"

  SAVEIFS=$IFS
  IFS=$(echo -en "\n\b")
  for font in `find $FONT_FILES -maxdepth 2 -name \*.ttf`; do
    dest="$HOME/Library/Fonts/`basename \"${font}\"`"
    if [ -f $dest ] || [ -d $dest ]; then
      e_success "skipped $dest because it already exists"
    else
      copy_files "$font" "$dest"
    fi
  done
  IFS=$SAVEIFS
}

if [[ "$OSTYPE" =~ ^darwin ]] ; then
  install_fontfiles
fi
